//1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// setTimeout() - виконує вказану функцію з вказаною затримкою один раз.
// setInterval()- виконує вказану функцію з вказаною затримкою багаторазово.
// Припинити виконання двох функцій можна відповідно за допомогою clearTimeout()
// та clearInterval().

//2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
// При цьому вказана в setTimeout() функція буде виконана максимально швидко, але після виконання
// поточного коду. Методи setTimeout()` і `setInterval(`)` не гарантують точну затримку виконання функції
// через ряд умов: обмеження стандарту HTML5, завантаження CPU тощо.

//3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
// Тому що функція продовжує працювати, використовуючи обчислювальні ресурси, пам'ять. Що може призвести до затримок в роботі.


let imgs = document.querySelectorAll("img");
let style = document.querySelector("style");

style.innerHTML += `
.hidden {
        display: none;
      }
@keyframes fadeout {
        0% {
          opacity: 0;
        }
        16.666% {
          opacity: 1;
        }
        83.333% {
          opacity: 1;
        }
        100%{
          opacity: 0;
        }
      }
      .fade{
        animation: fadeout 3s;
        animation-direction: normal;
        animation-timing-function: linear;
      }
`;

for (let i = 1; i < imgs.length; i++) { 
    imgs[i].classList.add('hidden');
}

let btn_pause = document.createElement("div");
btn_pause.textContent = "Припинити";
btn_pause.style.width = "150px";
btn_pause.style.height = "40px";
btn_pause.style.border = "1px solid black";
btn_pause.style.borderRadius = "3px";
btn_pause.style.display = "flex";
btn_pause.style.justifyContent = "center";
btn_pause.style.alignItems = "center";
btn_pause.style.cursor = "pointer";
document.body.append(btn_pause);

let btn_go = document.createElement("div");
btn_go.textContent = "Відновити показ";
btn_go.style.width = "150px";
btn_go.style.height = "40px";
btn_go.style.border = "1px solid black";
btn_go.style.borderRadius = "3px";
btn_go.style.display = "flex";
btn_go.style.justifyContent = "center";
btn_go.style.alignItems = "center";
btn_go.style.cursor = "pointer";
document.body.append(btn_go);

let time = document.createElement("div");
document.body.append(time);
let time2 = document.createElement("div");
document.body.append(time2);


let x;
let mins;
let ms;
let s1, s2;
let j = 1;
let go_check = 0;


btn_pause.onclick = () => { 
    if (x) { 
        clearInterval(x);
        clearInterval(mins);
        clearTimeout(ms);
        clearTimeout(s1);
        clearTimeout(s2);
        
        for (let i = 0; i < imgs.length; i++) { 
        imgs[i].classList.remove('fade');
        }
        // console.log("X");
        go_check = 1;
    }    
}


go();

btn_go.onclick = () => { 
    if (go_check === 1) { 
        go();
        go_check = 0;
        // console.log("GO");
    }    
}

function go() {
   
    time.textContent = `2s`;
    s1 = setTimeout(
        () => { time.textContent = `1s`; },
         1000
    );
            
    s2 = setTimeout(
        () => { time.textContent = `0s`; },
        2000
    );
    let mseconds = 99;
    setTimeout(
        function count() {
            time2.textContent = `${mseconds}0ms`;
            mseconds--;
            if (mseconds === -1) { 
            mseconds = 99;
            } 
            ms = setTimeout(count, 10);
        },
        0
    );
    
    x = setInterval(
        () => { 
                if (j >= imgs.length ) { 
                    j = 0;
                }
                for (let i = 0; i < imgs.length; i++) { 
                if (i === j) {
                    imgs[i].classList.remove("hidden");
                    imgs[i].classList.add("fade");
                }
                else { 
                    imgs[i].classList.add("hidden");
                    imgs[i].classList.remove("fade");
                }
            }      
            j++;
            go_check = 0;

            time2.textContent = `99ms`;
            mseconds = 99;
            
            clearTimeout(ms);
            
            setTimeout(
                function count() {
                    time2.textContent = `${mseconds}0ms`;
                    mseconds--;
                    if (mseconds === -1) { 
                    mseconds = 99;
                    } 
                    ms = setTimeout(count, 10);
                },
                0
            );

            
            time.textContent = `2s`;
            s1 = setTimeout(
                () => { time.textContent = `1s`; },
                1000
            );
            
            s2 = setTimeout(
                () => { time.textContent = `0s`; },
                2000
            );               
        },
        3000
    );
}
